import time
import random
import matplotlib.pyplot as plt


def with_qs(x):
    count = 0
    count_number = []
    for i in range(10):
        for j in range(1000):
            if i == x[j]:
                count += 1
        count_number.append(count)
        count = 0

    return dict((elem, count_number[elem]) for elem in range(10))


def pure_conversion(x):
    return dict((elem, x.count(elem)) for elem in set(x))


data_int = [random.randint(0, 10) for _ in range(1000)]

if int(input('Choose count method (1 - with pure conversion; 2 - with quicksort algorithm): ')) == 1:
    start_time = time.perf_counter()
    output1 = pure_conversion(data_int)
    print('Time = ', '{:f}'.format(float(time.perf_counter() - start_time)))
else:
    start_time = time.perf_counter()
    output1 = with_qs(data_int)
    print('Time = ', '{:f}'.format(float(time.perf_counter() - start_time)))

fig, ax = plt.subplots()

ax.bar(*zip(*sorted(output1.items())))

ax.set_autoscale_on(True)

ax.set_title("Integer")

plt.show()
