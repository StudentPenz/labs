import sys

# Обозначение переменной для общей суммы с начальным значением 42
total_sum = 42

# Цикл для считывания строк
for line in sys.stdin:
    # Условие для проверки строк
    if all(char.isdigit() for char in line.rstrip()) and (line.rstrip() != '' and line.rstrip() != '0'):
        total_sum += 1/(int(line))

# Вывод суммы
print(total_sum)
