import time
import sys


def output_string(index, input_lst):
    a = []
    for i in range(index, index + 23):
        a.append(input_lst[i])
    return "".join(a)


lines = []
for line in sys.stdin:
    lines.append(line)
gif = []
count = 0
while True:
    try:
        if lines[count].count("```") == 1:
            gif.append(output_string(count + 1, lines))
            count += 25
        elif lines[count].count("```") != 1:
            count += 1
    except IndexError:
        break

while True:
    try:
        for line in gif:
            time.sleep(0.5)
            print(end="")
            print(line)
    except KeyboardInterrupt:
        break
