def algorithm_saxpy(a, x, y):
    return [a * x_coord + y_coord for x_coord, y_coord in zip(x, y)]


type_choice = input('Выбор значений(float - 1, int - 2):')

if type_choice == '1':
    x_vector = list(map(float, input('Введите координаты вектора х через пробел: ').split()))
    y_vector = list(map(float, input('Введите координаты вектора y через пробел: ').split()))
    a = float(input('Введите значение скаляра: '))
elif type_choice == '2':
    x_vector = list(map(int, input('Введите координаты вектора х через пробел: ').split()))
    y_vector = list(map(int, input('Введите координаты вектора y через пробел: ').split()))
    a = int(input('Введите значение скаляра: '))
else:
    print('Неверный ввод')
    exit(1)

result = algorithm_saxpy(a, x_vector, y_vector)

print(result)
print(f"Результаты вычислений алгоритма SAXPY: {', '.join(map(str, result))}")
