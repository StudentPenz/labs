import time
import matplotlib.pyplot as plt
import random


def algorithm_saxpy(a, x, y):
    return [a * x_coord + y_coord for x_coord, y_coord in zip(x, y)]


time_int, data_int = [], []
for n in range(int(10e4), int(10e5), int(10e4)):
    x_coord = [random.randint(-100, 100) for i in range(n)]
    y_coord = [random.randint(-100, 100) for i in range(n)]
    a = random.randint(-100, 100)
    time_temp = 0
    for _ in range(4):
        start = time.time()
        saxpy = algorithm_saxpy(a, x_coord, y_coord)
        time_temp += time.time() - start
    data_int.append(n/int(10e4))
    time_int.append(time_temp/4)
    print(f'(int)Time for {n}: {time_temp/4}')

time_float, data_float = [], []
for n in range(int(10e4), int(10e5), int(10e4)):
    x_coord = [random.uniform(-100, 100) for i in range(n)]
    y_coord = [random.uniform(-100, 100) for i in range(n)]
    a = random.uniform(-100, 100)
    time_temp = 0
    for _ in range(4):
        start = time.time()
        saxpy = algorithm_saxpy(a, x_coord, y_coord)
        time_temp += time.time() - start
    data_float.append(n/int(10e4))
    time_float.append(time_temp/4)
    print(f'(float)Time for {n}: {time_temp/4}')

plt.plot(data_float, time_float, 'o')
plt.plot(data_float, time_float, 'b', c='blue', label="Тренд вычислений float")
plt.plot(data_int, time_int, 'o')
plt.plot(data_int, time_int, 'b', c='red', label="Тренд вычислений int")
plt.legend()
plt.xlabel('длина векторов*10^4')
plt.ylabel('время выполнения, с.')

plt.show()
