import time
import matplotlib.pyplot as plt
from random import randint, uniform


def matrix(a, b):
    return [[sum(x * y for x, y in zip(a_row, b_column)) for b_column in zip(*b)] for a_row in a]


n_min = 10e3
n_max = 10e4
n_step = 10e3

counter = 6

time_float, float_data = [], []
for n in range(int(n_min), int(n_max), int(n_step)):
    tmp_mat1 = [[uniform(1, 10) for j in range(counter)] for i in range(int(n))]
    tmp_mat2 = [[uniform(1, 10) for j in range(counter)] for i in range(int(n))]
    averages = []
    counter += 1
    for _ in range(3):
        start = time.time()
        temp1 = matrix(tmp_mat1, tmp_mat2)
        averages.append(time.time() - start)
    time_float.append(sum(averages) / 3)
    print(f'(float)Time for {n}: {time_float[-1]}')
    float_data.append(n/int(10e3))

counter = 6

time_int, int_data = [], []
for n in range(int(n_min), int(n_max), int(n_step)):
    tmp_mat1 = [[randint(1, 10) for j in range(counter)] for i in range(int(n))]
    tmp_mat2 = [[randint(1, 10) for j in range(counter)] for i in range(int(n))]
    averages = []
    counter += 1
    for _ in range(3):
        start = time.time()
        temp1 = matrix(tmp_mat1, tmp_mat2)
        averages.append(time.time() - start)
    time_int.append(sum(averages) / 3)
    print(f'(int)Time for {n}: {time_int[-1]}')
    int_data.append(n*counter/int(10e3))


plt.figure(figsize=(10,6))

ax = plt.subplot(211)
plt.plot(float_data, time_float, 'o')
plt.plot(float_data, time_float, c="blue", label=f"Тренд вычислений float()")

plt.legend()
plt.xlabel("Количество элементов в списке, *10^3")
plt.ylabel("Время вычисления, с.")
ax.set_xlim(1, 9)

ax = plt.subplot(212)
plt.plot(int_data, time_int, 'o')
plt.plot(int_data, time_int, c="red", label=f"Тренд вычислений int()")

plt.legend()
plt.xlabel("Количество элементов в списке, *10^3")
plt.ylabel("Время вычисления, с.")
ax.set_xlim(1, 9)
plt.show()

