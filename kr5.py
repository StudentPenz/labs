import time
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import random
import sys


def gauss_jordan(coeffs_matrix, answers_matrix):
    """
    Gauss-Jordan elimination algorithm

    :param coeffs_matrix: matrix of coefficients
    :param answers_matrix: matrix of answers
    :return <list>: list of all x's
    """

    def row_divider(mat1, mat2, row, divider):
        mat1[row] = [a / divider for a in mat1[row]]
        mat2[row] /= divider

    def row_comb(mat1, mat2, row, source_row, weight):
        mat1[row] = [(a + k * weight) for a, k in zip(mat1[row], mat1[source_row])]
        mat2[row] += mat2[source_row] * weight

    col = 0
    while col < len(answers_matrix):
        current_row = None
        for r in range(col, len(coeffs_matrix)):
            if current_row is None or abs(coeffs_matrix[r][col]) > abs(coeffs_matrix[current_row][col]):
                current_row = r

        if current_row is None:
            return None

        if current_row != col:
            coeffs_matrix[current_row], coeffs_matrix[col] = coeffs_matrix[col], coeffs_matrix[current_row]
            answers_matrix[current_row], answers_matrix[col] = answers_matrix[col], answers_matrix[current_row]

        row_divider(coeffs_matrix, answers_matrix, col, coeffs_matrix[col][col])

        for r in range(col + 1, len(coeffs_matrix)):
            row_comb(coeffs_matrix, answers_matrix, r, col, -coeffs_matrix[r][col])
        col += 1

    output = [0 for b in answers_matrix]
    for i in range(len(answers_matrix) - 1, -1, -1):
        output[i] = answers_matrix[i] - sum(x * a for x, a in zip(output[(i + 1):], coeffs_matrix[i][(i + 1):]))

    return output


n_min = 10
n_max = 100
n_step = 10

counter = 6
size_data = []
time_data = []
n_data = []


for n in range(int(n_min), int(n_max), int(n_step)):
    temp_mat1 = [[random.uniform(-10, 10) for j in range(counter)] for i in range(n)]
    temp_mat2 = [[random.uniform(-10, 10) for j in range(counter)] for i in range(n)]
    averages_time = []
    temp_size = []
    for _ in range(3):
        start = time.time()
        temp_size.append(sys.getsizeof(gauss_jordan(temp_mat1, temp_mat2)))
        averages_time.append(time.time() - start)
    time_data.append(sum(averages_time) / 3)
    size_data.append(sum(temp_size) / 3)
    print(time_data[-1])
    n_data.append(counter*n)
    counter += 1


fig = plt.figure()
ax = fig.add_subplot(111)
plt.plot(n_data, time_data, 'o')
plt.plot(n_data, time_data, 'b', c='red')
plt.xlabel('длина матриц')
plt.ylabel('время выполнения, c.')

fig = plt.figure()
ax = fig.add_subplot(111)
plt.plot(n_data, size_data, 'o')
plt.plot(n_data, size_data, 'b', c='green')
plt.xlabel('длина матриц')
plt.ylabel('объем матриц')

plt.show()
