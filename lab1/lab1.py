import math

x = float(input('Введите x для G: '))
a = float(input('Введите a для G: '))
print(f'G = {9 * (7 * a**2 + 39 * a * x + 20 * x**2)/(9 * a**2 + 59 * a * x + 30 * x**2)}')

x = float(input('Введите x для F: '))
a = float(input('Введите a для F: '))
print(f'F = {math.atanh(10 * a**2 - 43 * a * x + 28 * x**2)}')

x = float(input('Введите x для Y: '))
a = float(input('Введите a для Y: '))
print(f'Y = {math.log(-10 * a**2 - 27 * a * x + 28 * x**2 + 1)/(math.log(10))}')
