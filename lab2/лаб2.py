import math


# Блок для выбора функции и ввода переменных
try:
    number = int(input('Выберите номер функции(1 - G, 2 - F, 3 - Y):'))
    if number in [1, 2, 3]:
        try:
            x = float(input('Введите переменную x:'))
            a = float(input('Введите переменную a:'))
        except ValueError:
            print('Переменные - это числа')
            exit(1)
    else:
        print('Номер функции находится в диапазоне от 1 до 3')
        exit(0)
except ValueError:
    print('Номер функции - целое число')
    exit(1)

# Блок нахождения и вывода функции
if number == 1:
    try:
        G = 9 * (7 * a**2 + 39 * a * x + 20 * x**2)/(9 * a**2 + 59 * a * x + 30 * x**2)
        print("Функция G=" + str(G))
    except ZeroDivisionError:
        print('Функция G не имеет решение(деление на 0 невозможно)')
elif number == 2:
    try:
        F = math.atanh(10 * a**2 - 43 * a * x + 28 * x**2)
        print("Функция F=" + str(F))
    except ValueError:
        print('Функция F не имеет решения(аргумент не входит в область определения)')
elif number == 3:
    try:
        Y = math.log(-10 * a**2 - 27 * a * x + 28 * x**2 + 1)/(math.log(10))
        print("Функция Y=" + str(Y))
    except ValueError:
        print('Функция Y не имеет решения(аргумент не входят в ОДЗ')

