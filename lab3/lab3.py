from math import log, atanh
import matplotlib.pyplot as plt


def g(x, a):
    return float(9 * (7 * a ** 2 + 39 * a * x + 20 * x ** 2) / (9 * a ** 2 + 59 * a * x + 30 * x ** 2))


def f(x, a):
    return float(atanh(10 * a ** 2 - 43 * a * x + 28 * x ** 2))


def y(x, a):
    return float(log(-10 * a ** 2 - 27 * a * x + 28 * x ** 2 + 1) / (log(10)))


def make_graph(x_lst, y_lst):
    plt.title("График функции", fontsize=24)
    plt.xlabel("x", fontsize=14)
    plt.ylabel("f(x)", fontsize=14)
    plt.grid(which='major', c='black', linewidth=1)
    plt.plot(x_lst, y_lst, c='red')
    plt.show()


print('Зажмите Ctrl+C для выхода из функции')

while True:
    try:
        # Блок для выбора функции и ввода переменных
        try:
            number = int(input('Выберите номер функции(1 - G, 2 - F, 3 - Y):'))
            if number in [1, 2, 3]:
                x_max = float(input('Введите верхную границу X(число):'))
                x_min = float(input('Введите нижнюю границу X(число):'))
                if x_min > x_max:
                    print('Нижняя граница не может быть больше верхней')
                    exit(1)
                a = float(input('Введите переменную a(число):'))
                step_x = float(input('Введите шаг(число): '))
                if step_x < 0:
                    print('Шаг меньше нуля')
                    exit(1)
                elif step_x == 0:
                    print('Шаг равен нулю')
                    exit(1)
            else:
                print('Данного номера функции нет')
                exit(1)
        except ValueError:
            print('Ошибка ввода')
            exit(1)

        x_lst = []  # Лист для значений x
        y_lst = []  # Лист для значений f(x)

        # Заполнение листов
        if number == 1:
            while x_min < x_max:
                x_lst.append(x_min)
                try:
                    print(f'x = {x_min:.8f} | f(x) = {g(x_min, a):.8f}')
                    y_lst.append(g(x_min, a))
                except ZeroDivisionError:
                    print(f'x = {x_min:.8f} | f(x) = None')
                    y_lst.append(None)
                x_min += step_x
        elif number == 2:
            while x_min < x_max:
                x_lst.append(x_min)
                try:
                    print(f'x = {x_min:.8f} | f(x) = {f(x_min, a):.8f}')
                    y_lst.append(f(x_min, a))
                except ValueError:
                    print(f'x = {x_min:.8f} | f(x) = None')
                    y_lst.append(None)
                x_min += step_x
        elif number == 3:
            while x_min < x_max:
                x_lst.append(x_min)
                try:
                    print(f'x = {x_min:.8f} | f(x) = {y(x_min, a):.8f}')
                    y_lst.append(y(x_min, a))
                except ValueError:
                    print(f'x = {x_min:.8f} | f(x) = None')
                    y_lst.append(None)
                x_min += step_x

            # Построение графика
        make_graph(x_lst, y_lst)
    except KeyboardInterrupt:
        print('Выход из цикла')

    # Блок выхода
    if input('Для продолжения программы введите y:') == 'y' or 'Y':
        print('Программа продолжается')
    else:
        print('Программа завершилась')
        break
