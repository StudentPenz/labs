from math import log, atanh
import matplotlib.pyplot as plt


def g(x, a):
    return float(9 * (7 * a ** 2 + 39 * a * x + 20 * x ** 2) / (9 * a ** 2 + 59 * a * x + 30 * x ** 2))


def f(x, a):
    return float(atanh(10 * a ** 2 - 43 * a * x + 28 * x ** 2))


def y(x, a):
    return float(log(-10 * a ** 2 - 27 * a * x + 28 * x ** 2 + 1) / (log(10)))


print('Зажмите Ctrl+C для выхода из функции')

while True:
    try:
        try:
            number = int(input('Выберите номер функции(1 - G, 2 - F, 3 - Y):'))
            if number in [1, 2, 3]:
                x_max = float(input('Введите верхную границу X(число):'))
                x_min = float(input('Введите нижнюю границу X(число):'))
                if x_min > x_max:
                    print('Нижняя граница не может быть больше верхней')
                a = float(input('Введите переменную a(число):'))
                step_x = float(input('Введите шаг(число): '))
                if step_x <= 0:
                    print('Шаг меньше или равен нулю')
                    exit(0)
                template = float(input('Введите искомый шаблон: '))
            else:
                print('Данного номера функции нет')
                exit(1)
        except ValueError:
            print('Ошибка ввода')
            exit(1)

        x_lst = []
        y_lst = []
        hit = 0

        if number == 1:
            while x_min < x_max:
                x_lst.append(x_min)
                try:
                    y_lst.append(g(x_min, a))
                except ZeroDivisionError:

                    y_lst.append(None)
                x_min += step_x
        elif number == 2:
            while x_min < x_max:
                x_lst.append(x_min)
                try:
                    y_lst.append(f(x_min, a))
                except ValueError:
                    y_lst.append(None)
                x_min += step_x
        elif number == 3:
            while x_min < x_max:
                x_lst.append(x_min)
                try:
                    y_lst.append(y(x_min, a))
                except ValueError:
                    y_lst.append(None)
                x_min += step_x

        output = ''
        for i in range(len(y_lst)):
            output += str(y_lst[i]) + ' '
            if y_lst[i] == template:
                hit += 1

        print(f'f(x) - {output}')
        print(f'Количество шаблона в списке: {hit}')
    except KeyboardInterrupt:
        print('\nВыход из цикла')

    if input('Для продолжения программы введите y: ') == 'y' or 'Y':
        print('Программа продолжается')
    else:
        print('Программа завершилась')
        break
