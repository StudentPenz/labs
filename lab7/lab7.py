from math import log, atanh


def g(x, a):
    return float(9 * (7 * a ** 2 + 39 * a * x + 20 * x ** 2) / (9 * a ** 2 + 59 * a * x + 30 * x ** 2))


def f(x, a):
    return float(atanh(10 * a ** 2 - 43 * a * x + 28 * x ** 2))


def y(x, a):
    return float(log(-10 * a ** 2 - 27 * a * x + 28 * x ** 2 + 1) / (log(10)))


print('Зажмите Ctrl+C для выхода из функции')

while True:
    try:
        try:
            x_max = float(input('Введите верхную границу X(число):'))
            x_min = float(input('Введите нижнюю границу X(число):'))
            a = float(input('Введите переменную a(число):'))
            step_x = float(input('Введите шаг(число): '))
        except ValueError:
            print('Ошибка ввода')
            exit(1)

        x_lst = []
        y_lst = [[], [], []]

        while x_min < x_max:
            x_lst.append(x_min)
            try:
                y_lst[0].append(g(x_min, a))
            except ZeroDivisionError:
                y_lst[0].append(None)
            try:
                y_lst[1].append(f(x_min, a))
            except ValueError:
                y_lst[1].append(None)
            try:
                y_lst[2].append(y(x_min, a))
            except ValueError:
                y_lst[2].append(None)
            x_min += step_x

        file = open('lab7.txt', 'w')
        file.write(f'{y_lst[0]}\n{y_lst[1]}\n{y_lst[2]}\n')
        file.close()

        y_lst = []

        file = open('lab7.txt', 'r')
        for line in file:
            y_lst.append(line.split())
        file.close()

        for i in range(len(y_lst[0])):
            print(f'x = {x_lst[i]} | g(x) = {y_lst[0][i]}\n')

        for i in range(len(y_lst[1])):
            print(f'x = {x_lst[i]} | f(x) = {y_lst[1][i]}\n')

        for i in range(len(y_lst[2])):
            print(f'x = {x_lst[i]} | y(x) = {y_lst[2][i]}\n')

    except KeyboardInterrupt:
        print('Выход из цикла')

    if input('Для продолжения программы введите y: ') == 'y' or 'Y':
        print('Программа продолжается')
    else:
        print('Программа завершилась')
        break
