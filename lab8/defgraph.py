import matplotlib.pyplot as plt
import random
import time


def delta(cen, x, y, r):
    return ((cen[0] - x) ** 2 + (cen[1] - y) ** 2) < (r ** 2)


def make_graph(list1, list2):
    plt.plot(list1, list2, 'o')
    plt.plot(list1, list2, c='red')
    plt.title("График")
    plt.xlabel("Кол-во точек")
    plt.ylabel("Время выполнения, сек.")
    plt.show()


i_max = 1000000
i_min = 100000
step = 100000
scatter_area, time_temp = 0, 0
x1, x2, y1, y2 = [], [], [], []
time_data = []
i_data = []

for s in range(i_min, i_max, step):

    r = s / 2
    i_lst = [(random.uniform(-10, 10) for i in range(s))]
    center = (random.uniform(i_min, i_max), random.uniform(i_min, i_max))

    for n in range(4):

        time_start = time.time()

        for i in range(s):
            if delta(center, i_lst, r) is True:
                scatter_area += 1

        time_temp += time.time()-time_start

    print(time_temp / 4)
    time_data.append(time_temp/4)
    i_data.append(s)
    time_temp = 0

make_graph(i_data, time_data)
