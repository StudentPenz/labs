import random
import matplotlib.pyplot as plt
import matplotlib.patches as pch


def delta(cen, x, y, rad):
    return ((cen[0] - x) ** 2 + (cen[1] - y) ** 2) < (rad ** 2)


def make_graph(cen, rad, x1, x2, y1, y2):
    ax = plt.subplot()
    plt.title("График функции")
    plt.xlabel("x")
    plt.ylabel("f(x)")
    ax.scatter(x1, y1, c='purple', zorder=1)
    ax.scatter(x2, y2, c='red', zorder=0)
    ax.scatter(cen[0], cen[1], c='black', zorder=2)
    ax.add_patch(pch.Circle((cen[0], cen[1]), radius=rad, fill=False))
    ax.axis('square')
    ax.set_xlim(0, 100)
    ax.set_ylim(0, 100)
    plt.show()


lot = int(input('Введите кол-во точек: '))
r = float(input('Введите радиус окружности: '))

x_lst, y_lst = [], []

for i in range(lot):
    x_lst.append(random.uniform(0, 100))
    y_lst.append(random.uniform(0, 100))

center = (random.uniform(0, 100), random.uniform(0, 100))

scatter_area = 0

x1, x2, y1, y2 = [], [], [], []

for i in range(lot):
    if delta(center, x_lst[i], y_lst[i], r) is True:
        scatter_area += 1
        x1.append(x_lst[i])
        y1.append(y_lst[i])
    else:
        x2.append(x_lst[i])
        y2.append(y_lst[i])

print(f'Количество точек в окр.: {scatter_area}')

make_graph(center, r, x1, x2, y1, y2)
